﻿namespace ApiGDTVietnam.Common.QuanLyBanHangConstants
{
    public static class BanHangConst
    {
        /// <summary>
        /// Thẻ kho _ loại và hình thức.
        /// </summary>
        public const string TK_LOAI_NHAP = "PN";
        public const string TK_LOAI_XUAT = "PX";
        public const string TK_LOAI_DONHANG = "DH";

        public const string TK_HINHTHUC_NCC = "Nhập hàng";
        public const string TK_HINHTHUC_DONHANG = "Bán hàng";
        public const string TK_HINHTHUC_TRAHANG = "Trả hàng";
        public const string TK_HINHTHUC_HUYTRAHANG = "Hủy đơn trả hàng";
        public const string TK_HINHTHUC_CHUYENKHO = "Chuyển kho";
        public const string TK_HINHTHUC_KIEMKHO = "Kiểm kho";

        /// <summary>
        /// Kho_ loại
        /// </summary>
        public const string K_LOAI_NCC = "NCC";
        public const string K_LOAI_BANHANG = "ban-hang";
        public const string K_LOAI_CHUYENKHO = "chuyen-kho";
        public const string K_LOAI_TRA = "tra";
        public const string K_LOAI_KIEMKHO = "kiem-kho";

        /// <summary>
        /// Chuyển kho _ trạng thái
        /// </summary>
        public const string CK_TRANGTHAI_YEUCAUMOI = "yeu_cau_moi";
        public const string CK_TRANGTHAI_DAXUATKHO = "da_xuat_kho";
        public const string CK_TRANGTHAI_TUCHOI = "tu_choi";
        public const string CK_TRANGTHAI_DANHAPKHO = "da_nhap_kho";

        /// <summary>
        /// Kiểm kho _ trạng thái.
        /// </summary>
        public const string KK_TRANGTHAI_HOANTHANH = "hoan_thanh";
        public const string KK_TRANGTHAI_LUUTAM = "luu_tam";
        public const string KK_TRANGTHAI_DAHUY = "da-huy";

        /// <summary>
        /// Mã chúng từ
        /// </summary>
        public const string MCT_XUATHANG = "PX";
        public const string MCT_NHAPHANG = "PN";
        public const string MCT_CHUYENKHO = "CK";
        public const string MCT_BANHANG = "BH";
        public const string MCT_PHIEUTHU = "PT";
        public const string MCT_TRAHANG = "TH";
        public const string MCT_PHIEUCHI = "PC";
        public const string MCT_KIEMKHO = "KK";
        public const string MCT_MUAHANG = "MH";

        public const string SP_LOAI_SANPHAM = "SanPham";
        public const string SP_LOAI_DICHVU = "DichVu";
        public const string SP_LOAI_THANHPHAM = "ThanhPham";
    }
}