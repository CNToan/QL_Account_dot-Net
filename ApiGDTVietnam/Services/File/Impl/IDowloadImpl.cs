﻿using ApiGDTVietnam.Ultility.BaseObject;

using DocumentFormat.OpenXml.Packaging;
using HtmlToOpenXml;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using openXmlWord = DocumentFormat.OpenXml.Wordprocessing;

namespace ApiGDTVietnam.Controllers.Report.Service
{
    public class IDowloadImpl : ApiController, IDowloadService
    {
        public string  RenderReportHtml(string folder, object model)
        {
            string fileIndex = HttpContext.Current.Server.MapPath("~/template/html/" + folder + "/index.cshtml");
            string fileDetail = HttpContext.Current.Server.MapPath("~/template/html/" + folder + "/detail.cshtml");
            string fileHeader = HttpContext.Current.Server.MapPath("~/template/html/header.cshtml");
            string fileFooter = HttpContext.Current.Server.MapPath("~/template/html/footer.cshtml");
            string fileHeaderPhieu = HttpContext.Current.Server.MapPath("~/template/html/headerphieu.cshtml");
            string fileFooterPhieu = HttpContext.Current.Server.MapPath("~/template/html/footerphieu.cshtml");
            string rs = "";
            if (File.Exists(fileIndex))
            {
                var templateIndex = Regex.Replace(File.ReadAllText(fileIndex, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");
                var templateDetail = Regex.Replace(File.ReadAllText(fileDetail, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");
                var templateHeader = Regex.Replace(File.ReadAllText(fileHeader, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");
                var templateFooter = Regex.Replace(File.ReadAllText(fileFooter, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");

                var templateHeaderPhieu = Regex.Replace(File.ReadAllText(fileHeaderPhieu, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");
                var templateFooterPhieu = Regex.Replace(File.ReadAllText(fileFooterPhieu, System.Text.Encoding.UTF8), @"[\n\t\r]+", "");

                templateIndex = templateIndex.Replace("{detail.cshtml}", templateDetail);
                templateIndex = templateIndex.Replace("{header.cshtml}", templateHeader);
                templateIndex = templateIndex.Replace("{footer.cshtml}", templateFooter);
                templateIndex = templateIndex.Replace("{headerphieu.cshtml}", templateHeaderPhieu);
                templateIndex = templateIndex.Replace("{footerphieu.cshtml}", templateFooterPhieu);

                if (Engine.Razor.IsTemplateCached(folder, null))
                {
                    rs = Engine.Razor.Run(folder, null, model);
                }
                else
                    rs = Engine.Razor.RunCompile(templateIndex, folder, null, model);
            }

            return rs;
        }
        public HttpResponseMessage Download(string html, XDocument xml, Rectangle pageZize, string mode, string filename, bool logo = true)
        {
            switch (mode)
            {
                case "download_excel":
                    return DownloadExcel(html, filename + ".xlsx");
                case "download_word":
                    return DownloadWord(html, filename + ".docx", false);
                case "download_xml":
                    return DownloadXml(xml, filename + ".xml");
                default:
                    return DownloadPDF(html, pageZize, filename + ".pdf", mode, logo);
            }
        }
        private HttpResponseMessage DownloadXml(XDocument xml, string filename)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            using (var stream = new MemoryStream())
            {
                xml.Save(stream);
                result.Content = new ByteArrayContent(stream.ToArray());
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = filename
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
            }
            return result;
        }

        private HttpResponseMessage DownloadPDF(string html, Rectangle pageSize, string filename, string mode = "download", bool logo = true)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    convertHtmlToPdf(stream, html, pageSize, logo);

                    switch (mode)
                    {
                        case "html":
                            return Request.CreateResponse(HttpStatusCode.OK, html);
                        case "download_pdf":
                            var result = new HttpResponseMessage(HttpStatusCode.OK);
                            result.Content = new ByteArrayContent(stream.ToArray());
                            result.Content.Headers.ContentDisposition =
                                                    new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                                                    {
                                                        FileName = filename
                                                    };
                            return result;
                        case "blob":
                            result = new HttpResponseMessage(HttpStatusCode.OK);
                            result.Content = new ByteArrayContent(stream.ToArray());
                            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                            result.Content.Headers.ContentLength = stream.Length;

                            ContentDispositionHeaderValue contentDisposition = null;
                            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename, out contentDisposition))
                            {
                                result.Content.Headers.ContentDisposition = contentDisposition;
                            }
                            return result;
                        case "base64":
                            string pdfBase64 = Convert.ToBase64String(stream.ToArray());
                            return Request.CreateResponse(HttpStatusCode.OK, pdfBase64);
                        default:
                            return Request.CreateResponse(HttpStatusCode.OK, "Không tạo được báo cáo 1");
                    }
                }
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Không tạo được báo cáo 2");
            }
        }

        private HttpResponseMessage DownloadExcel(string html, string filename)
        {
            //Stream  
            TableToExcel temp = new TableToExcel();
            byte[] buf = temp.process(HttpUtility.HtmlDecode(html));

            //var stream = new MemoryStream();
            //var fileStream = File.OpenRead(path);

            //fileStream.CopyTo(stream);
            //byte[] buf = new byte[stream.Length];
            //stream.Read(buf, 0, buf.Length);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(buf);
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = filename
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            //fileStream.Dispose();
            //stream.Dispose();
            return result;
        }

        private HttpResponseMessage DownloadWord(string html, string filename, bool landscape = false)
        {
            string tmplDocx = HttpContext.Current.Server.MapPath("~/Uploads/BaoCao/Template.docx");
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            byte[] docxByte = File.ReadAllBytes(tmplDocx);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(docxByte, 0, docxByte.Length);
                using (WordprocessingDocument package = WordprocessingDocument.Open(stream, true))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new openXmlWord.Document(new openXmlWord.Body()).Save(mainPart);
                    }
                    HtmlConverter converter = new HtmlConverter(mainPart);
                    mainPart.Document.Body.Append(converter.Parse(html));
                    mainPart.Document.Save();
                    stream.Capacity = (int)stream.Length;
                    response.Content = new ByteArrayContent(stream.ToArray());
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = Path.GetFileName(filename) };
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                }
            }
            return response;
        }
        private void convertHtmlToPdf(Stream stream, string html, Rectangle pageSize, bool logo = true)
        {
            if (!string.IsNullOrEmpty(html))
            {
                FontFactory.Register(HttpContext.Current.Server.MapPath("~/template/_font/times/times.ttf"));
                var unicodeFontProvider = FontFactoryImp.Instance;
                unicodeFontProvider.DefaultEmbedding = BaseFont.EMBEDDED;
                unicodeFontProvider.DefaultEncoding = BaseFont.IDENTITY_H;

                var props = new Hashtable
                {
                    //{ "img_provider", new MyImageFactory() },
                    { "font_factory", unicodeFontProvider } // Always use Unicode fonts
                };

                // step 1
                var document = new Document(pageSize);
                // step 2
                PdfWriter.GetInstance(document, stream);
                // step 3
                document.AddAuthor("GDT VietNam Company");

                // we Add a Footer that will show up on PAGE 1
                HeaderFooter footer = new HeaderFooter(new Phrase("Trang "), true);
                footer.Border = Rectangle.NO_BORDER;
                footer.Alignment = HeaderFooter.ALIGN_RIGHT;
                document.Footer = footer;

                document.Open();
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                // step 4
                var objects = HtmlWorker.ParseToList(
                    new StringReader(html),
                    loadStyles(),
                    props
                );

                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value.
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Console.WriteLine("RunTime " + elapsedTime);

                int line = 0;
                if (logo)
                {
                    //document.Add(Helper.buildLogo(30, 1070));
                }
                foreach (IElement element in objects)
                {
                    var type = element.GetType();
                    switch (element.Type)
                    {
                        case 12://Paragraph
                            Paragraph p = element as Paragraph;
                            if (p.Content == "page_break")
                            {
                                document.NewPage();
                            }
                            else
                            {
                                if (line == 1)
                                {
                                    p.SpacingBefore = 20;
                                }
                                p.SpacingAfter = 10;
                                document.Add(p);
                            }
                            break;
                        case 23://Table                        
                            document.Add(element);
                            break;
                        default:
                            document.Add(element);
                            break;
                    }

                    line++;
                }

                document.Close();
            }
        }

        private static StyleSheet loadStyles()
        {
            var styles = new StyleSheet();
            // set the default font's properties
            styles.LoadTagStyle(HtmlTags.BODY, "encoding", "Identity-H");
            styles.LoadTagStyle(HtmlTags.BODY, HtmlTags.FONT, "times new roman");
            styles.LoadTagStyle(HtmlTags.BODY, "size", "12pt");

            return styles;
        }

        static string getPathTempFilePdf()
        {
            //Tạo thư mục uploads nếu chưa có
            string upload_folder = HttpContext.Current.Server.MapPath("~/Uploads/");
            if (!Directory.Exists(upload_folder))
            {
                Directory.CreateDirectory(upload_folder);
            }

            //Tạo thư mục cho đơn vị
            string temppdf_folder = Path.Combine(upload_folder, "temppdf");
            if (!Directory.Exists(temppdf_folder))
            {
                Directory.CreateDirectory(temppdf_folder);
            }

            string file_name = Guid.NewGuid().ToString();

            return Path.Combine(temppdf_folder, file_name + ".pdf");
        }

    }
}