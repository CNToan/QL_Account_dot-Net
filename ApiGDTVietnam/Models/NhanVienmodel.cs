﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiGDTVietnam.Models
{
    public class NhanVienmodel
    {
        public Guid? id { get; set; }

        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string GioiTinh { get; set; }
        public List<NhanVienmodel> children { get; set; }
    }
}