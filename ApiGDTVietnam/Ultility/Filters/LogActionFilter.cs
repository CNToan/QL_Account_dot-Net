﻿using ApiGDTVietnam.Controllers;
using ApiGDTVietnam.DataProvider.EF;
using ApiGDTVietnam.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Script.Serialization;

namespace ApiGDTVietnam.Ultility
{
    public class LogAttribute : ActionFilterAttribute
    {
        public LogAttribute()
        {

        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var controllerName = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var actionName = actionContext.ActionDescriptor.ActionName;
            string Method = actionContext.Request.Method.Method;

            string rawRequest = null;
            if (HttpContext.Current.Request.Files.Count == 0)
            {
                using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result))
                {
                    stream.BaseStream.Position = 0;
                    rawRequest = stream.ReadToEnd();
                }
            }
            string Url = actionContext.Request.RequestUri.OriginalString;
            var ip = GetClientIpAddress(actionContext.Request);

            try
            {
                //if (checkForLog(controllerName, actionName, Method))
                if (Method != "GET")
                {
                    log(controllerName, actionName, Method, rawRequest, Url, ip);
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        //private bool checkForLog(string controllerName, string actionName, string Method)
        //{
        //    if (Method != "GET")
        //    {
        //        switch (actionName)
        //        {
        //            case "Post":
        //            case "Patch":
        //            case "Delete":
        //            case "Authenticate":
        //            case "LogOff":
        //                return true;
        //        }
        //    }

        //    return false;
        //}

        private void log(string controllerName, string actionName,
            string method, string json, string url, string ip)
        {
            string MaTaiKhoan = "";
            if (controllerName == "Login" && actionName == "Authenticate")
            {
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                LoginRequest login = json_serializer.Deserialize<LoginRequest>(json);

                MaTaiKhoan = login.Username;
            }
            else
            {
                if (controllerName == "HoSoCSDLGia" && actionName == "KySo")
                {
                    MaTaiKhoan = HttpContext.Current.Request["ma_tai_khoan"];
                }
                else
                {
                    MaTaiKhoan = BaseController.MaTaiKhoan;
                }
                
            }

            //Lưu log
            using (AccountContext db = new AccountContext())
            {
                SYS_NhatKyNguoiDung sYS_NhatKyNguoiDung = new SYS_NhatKyNguoiDung();
                sYS_NhatKyNguoiDung.Uuid = Guid.NewGuid().ToString();
                sYS_NhatKyNguoiDung.TaiKhoan_Id = MaTaiKhoan;
                sYS_NhatKyNguoiDung.ThoiGian = DateTime.Now;
                sYS_NhatKyNguoiDung.ControllerName = controllerName;
                sYS_NhatKyNguoiDung.ActionName = actionName;
                sYS_NhatKyNguoiDung.Method = method;
                sYS_NhatKyNguoiDung.Url = url;
                sYS_NhatKyNguoiDung.Json = json;
                sYS_NhatKyNguoiDung.IP = ip;
                sYS_NhatKyNguoiDung.Action_Id = getAction(db, controllerName, actionName, method, json, url);

                db.SYS_NhatKyNguoiDung.Add(sYS_NhatKyNguoiDung);
                db.SaveChanges();
            }
        }

        private Guid getAction(Acc db, string controllerName, string actionName,
            string Method, string rawRequest, string Url)
        {
            //Lấy module
            var module = getModule(db, controllerName, actionName, rawRequest, Url);

            if (module == null)
            {
                var action = db.SYS_Action.FirstOrDefault(item => item.ControllerName == controllerName
                        && item.ActionName == actionName);

                if (action != null)
                {
                    return action.ID;
                }
            }
            else
            {
                var action = db.SYS_Action.FirstOrDefault(item => item.ModuleId == module.id
                        && item.ControllerName == controllerName
                        && item.ActionName == actionName);

                if (action != null)
                {
                    return action.ID;
                }
            }

            SYS_Action new_action = new SYS_Action();
            new_action.ID = Guid.NewGuid();
            new_action.ControllerName = controllerName;
            new_action.ActionName = actionName;

            string module_name = "";
            if (module != null)
            {
                module_name = " \"" + (string.IsNullOrEmpty(module.name) ? module.table_name : module.name) + "\"";
            }

            if (string.IsNullOrEmpty(module_name))
            {
                module_name = controllerName;
            }

            new_action.Name = getTitle(actionName) + (controllerName == "HoSoes" || controllerName == "HoSoCSDLGia" ? " hồ sơ" : "") + module_name;
            if (module != null)
            {
                new_action.ModuleId = module.id;
            }

            db.SYS_Action.Add(new_action);
            db.SaveChanges();
            return new_action.ID;
        }

        private SYS_Module getModule(Entities db, string controllerName, string actionName,
            string rawRequest, string Url)
        {
            SYS_Module module = null;
            try
            {
                if (controllerName == "HoSoes")
                {
                    module = getModuleHoSoes(db, actionName, rawRequest, Url);
                }
                else if(controllerName == "HoSoCSDLGia")
                {
                    module = getHoSoCSDLGia(db, actionName, rawRequest, Url);
                }
                else
                {
                    module = db.SYS_Module.SingleOrDefault(item => item.api == controllerName);
                }
            }
            catch { }

            return module;
        }

        private SYS_Module getModuleHoSoes(Entities db, string actionName, string rawRequest, string url)
        {
            SYS_Module module = null;
            switch (actionName)
            {
                //case "GetHoSoes":
                //    int s = sYS_NhatKyNguoiDung.Url.IndexOf("CSDLG_");
                //    if (s > 0)
                //    {
                //        string loaiHoSo = sYS_NhatKyNguoiDung.Url.Substring(s, sYS_NhatKyNguoiDung.Url.Length - s - 3);
                //        module = db.SYS_Module.SingleOrDefault(item => item.module_name == loaiHoSo);
                //    }
                //    break;
                case "Post":
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    HoSo HoSo = json_serializer.Deserialize<HoSo>(rawRequest);
                    module = db.SYS_Module.SingleOrDefault(item => item.module_name == HoSo.LoaiHoSo);
                    break;
                case "Patch":
                case "Delete":
                    int s = url.IndexOf("('");
                    if (s > 0)
                    {
                        string ID = url.Substring(s + 2, url.Length - s - 4);
                        if (!string.IsNullOrEmpty(ID))
                        {
                            string loaiHoSo = db.HoSoes.SingleOrDefault(item => item.ID == ID).LoaiHoSo;
                            module = db.SYS_Module.SingleOrDefault(item => item.module_name == loaiHoSo);
                        }
                    }
                    break;
            }

            return module;
        }

        private SYS_Module getHoSoCSDLGia(Entities db, string actionName, string rawRequest, string url)
        {
            SYS_Module module = null;
            switch (actionName)
            {
                case "KySo":
                    string id_ho_so = HttpContext.Current.Request["id_ho_so"];
                    string loaiHoSo = db.HoSoes.SingleOrDefault(item => item.ID == id_ho_so).LoaiHoSo;
                    module = db.SYS_Module.SingleOrDefault(item => item.module_name == loaiHoSo);
                    break;
                case "TuChoiHoSo":
                case "GuiHoSo":
                case "TrinhKy":
                case "DuyetHoSo":
                case "CongBo":
                case "HuyCongBo":
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    CSDLGUpdateStatusModel model = json_serializer.Deserialize<CSDLGUpdateStatusModel>(rawRequest);
                    loaiHoSo = db.HoSoes.SingleOrDefault(item => item.ID == model.id).LoaiHoSo;
                    module = db.SYS_Module.SingleOrDefault(item => item.module_name == loaiHoSo);
                    break;
            }

            return module;
        }

        private string getTitle(string actionName)
        {
            switch (actionName)
            {
                case "Post":
                    return "Thêm";
                case "Patch":
                    return "Sửa";
                case "Delete":
                    return "Xóa";
                case "Authenticate":
                    return "Đăng nhập";
                case "LogOff":
                    return "Đăng xuất";
                case "KySo":
                    return "Ký số";
                case "TuChoiHoSo":
                    return "Từ chối";
                case "GuiHoSo":
                    return "Gửi";
                case "TrinhKy":
                    return "Trình ký";
                case "DuyetHoSo":
                    return "Duyệt";
                case "CongBo":
                    return "Công bố";
                case "HuyCongBo":
                    return "Hủy công bố";
            }

            return actionName;
        }

        private string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
            }
            return String.Empty;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
        }
    }
}