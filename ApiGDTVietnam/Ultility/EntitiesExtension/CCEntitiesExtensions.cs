﻿namespace ApiGDTVietnam.DataProvider.EF
{
    /* public partial class CCEntities : DbContext
     {
         //public CCEntities(string userName = null) : base("name=CCEntities")
         //{
         //    UserName = !string.IsNullOrEmpty(userName) ? userName : "Anonymous";
         //}

         public string UserName
         {
             get; private set;
         }
         public string DonVi
         {
             get; private set;
         }
         public override int SaveChanges()
         {
             try
             {
                 SavingChanges();
                 return base.SaveChanges();
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }
         public int SaveChangesWithCache(string className, bool isDonVi = false)
         {
             try
             {
                 CacheWithEntity cache = SavingChanges(true, className, isDonVi);
                 var a = base.SaveChanges();
                 if (HttpContext.Current.Request.HttpMethod == "DELETE")
                 {
                     DictionaryStore.Remove(cache.name, cache.value);
                 }
                 else
                 {
                     DictionaryStore.Add(cache.name, cache.value);
                 }
                 return a;
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }
         public override async Task<int> SaveChangesAsync()
         {
             try
             {
                 SavingChanges();
                 return await base.SaveChangesAsync();
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }
         public async Task<int> SaveChangesAsyncWithCache()
         {
             try
             {
                 CacheWithEntity cache = SavingChanges(true);
                 var a = await base.SaveChangesAsync();
                 if (HttpContext.Current.Request.HttpMethod == "DELETE")
                 {
                     DictionaryStore.Remove(cache.name, cache.value);
                 }
                 else
                 {
                     DictionaryStore.Add(cache.name, cache.value);
                 }
                 return a;
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }

         public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
         {
             try
             {
                 SavingChanges();
                 return await base.SaveChangesAsync(cancellationToken);
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }
         public async Task<int> SaveChangesAsyncWithCache(CancellationToken cancellationToken)
         {
             try
             {
                 SavingChanges(true);
                 CacheWithEntity cache = SavingChanges(true);
                 var a = await base.SaveChangesAsync(cancellationToken);
                 if (HttpContext.Current.Request.HttpMethod == "DELETE")
                 {
                     DictionaryStore.Remove(cache.name, cache.value);
                 }
                 else
                 {
                     DictionaryStore.Add(cache.name, cache.value);
                 }
                 return a;
             }
             catch
             {
                 throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
             }
         }

         private CacheWithEntity SavingChanges(bool cache = false, string className = null, bool isDonVi = false)
         {
             using (var OC = new CCEntities())
             {
                 CacheWithEntity model = new CacheWithEntity();
                 var objects = this.ChangeTracker.Entries()
                     .Where(p => p.State == EntityState.Added ||
                         p.State == EntityState.Deleted ||
                         p.State == EntityState.Modified);
                 var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
                 UserName = identity?.FindFirst("ma_tai_khoan")?.Value ?? (identity?.IsAuthenticated == true ? "Anonymous" : "Public");
                 DonVi = identity?.FindFirst("id_don_vi")?.Value ?? (identity?.IsAuthenticated == true ? "Anonymous" : "Public");
                 // handle auditing
                 //AuditingHelperUtility.ProcessAuditFields(
                 //    objects.Where(p => p.State == EntityState.Added));
                 //AuditingHelperUtility.ProcessAuditFields(
                 //    objects.Where(p => p.State == EntityState.Modified), InsertMode: false);

                 //    var changeInfo = OC.ChangeTracker.Entries()
                 //.Where(t => t.State == EntityState.Modified)
                 //.Select(t => new {
                 //    Original = t.OriginalValues.PropertyNames.ToDictionary(pn => pn, pn => t.OriginalValues[pn]),
                 //    Current = t.CurrentValues.PropertyNames.ToDictionary(pn => pn, pn => t.CurrentValues[pn]),
                 //});

                 ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;
                 foreach (DbEntityEntry entry in objects
                     .Where(p => p.State == EntityState.Added))
                 {
                     if (entry.Entity != null)
                     {
                         //Lấy primary key
                         var a = entry.Entity.GetType();
                         string name = a.Name;
                         MethodInfo method = typeof(ObjectContext).GetMethod("CreateObjectSet", Type.EmptyTypes)
                         .MakeGenericMethod(a);
                         dynamic objectSet = method.Invoke(objectContext, null);
                         IEnumerable<dynamic> keyMembers = objectSet.EntitySet.ElementType.KeyMembers;
                         string keyNames = keyMembers.Select(k => (string)k.Name).First();

                         Guid guid = Guid.NewGuid();
                         if (keyNames == "ID")
                         {
                             a.GetProperty(keyNames).SetValue(entry.Entity, guid);
                         }
                         a.GetType().GetProperty("TrangThai")?.SetValue(a, true);
                         a.GetProperty("Created_By").SetValue(entry.Entity, UserName);
                         a.GetProperty("Created_Date").SetValue(entry.Entity, DateTime.Now);
                         a.GetProperty("DV_ID").SetValue(entry.Entity, DonVi);
                         if (cache && name == className)
                         {
                             model.name = !isDonVi ? name : (name + DonVi);
                             CacheModel cacheModel = new CacheModel()
                             {
                                 k = guid.ToString(),
                                 v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                             };
                             model.value = cacheModel;
                             //DictionaryStore.Add(name, model);
                         }
                     }
                 }

                 foreach (DbEntityEntry entry in objects
                     .Where(p => p.State == EntityState.Modified))
                 {
                     if (entry.Entity != null)
                     {
                         var a = entry.Entity.GetType().BaseType;
                         string name = a.Name;
                         if ((entry.Property("Created_By") != null && entry.Property("Created_By").IsModified) ||
                             (entry.Property("Created_Date") != null && entry.Property("Created_Date").IsModified))
                         {
                             throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
                         }

                         MethodInfo method = typeof(ObjectContext).GetMethod("CreateObjectSet", Type.EmptyTypes)
                         .MakeGenericMethod(a);
                         dynamic objectSet = method.Invoke(objectContext, null);
                         IEnumerable<dynamic> keyMembers = objectSet.EntitySet.ElementType.KeyMembers;
                         string keyNames = keyMembers.Select(k => (string)k.Name).First();

                         if (HttpContext.Current.Request.HttpMethod == "DELETE")
                         {
                             a.GetProperty("Deleted_By").SetValue(entry.Entity, UserName);
                             a.GetProperty("Deleted_Date").SetValue(entry.Entity, DateTime.Now);
                             if (cache && name == className)
                             {
                                 model.name = !isDonVi ? name : (name + DonVi);
                                 CacheModel cacheModel = new CacheModel()
                                 {
                                     k = a.GetProperty(keyNames).GetValue(entry.Entity).ToString(),
                                     v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                                 };
                                 model.value = cacheModel;
                                 //DictionaryStore.Remove(name,model);
                             }
                         }
                         else
                         {
                             //var b = a.GetProperties().Where(m => m.GetGetMethod().IsVirtual && !m.GetGetMethod().IsFinal).ToList();
                             //b.ForEach(m =>
                             //{
                             //    Type c = m.PropertyType;
                             //    MethodInfo method1 = typeof(ObjectContext).GetMethod("CreateObjectSet", Type.EmptyTypes).MakeGenericMethod(c);
                             //    dynamic objectSet1 = method1.Invoke(objectContext, null);
                             //    var d = objectSet1.EntitySet;
                             //    var e = a.GetProperty("SanPham_DuAn").GetValue(a);
                             //    //DictionaryStore.Get<>
                             //});
                             a.GetProperty("Updated_By").SetValue(entry.Entity, UserName);
                             a.GetProperty("Updated_Date").SetValue(entry.Entity, DateTime.Now);
                             if (cache && name == className)
                             {
                                 model.name = !isDonVi ? name : (name + DonVi);
                                 CacheModel cacheModel = new CacheModel()
                                 {
                                     k = a.GetProperty(keyNames).GetValue(entry.Entity).ToString(),
                                     v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                                 };
                                 model.value = cacheModel;
                                 //model.value.k = a.GetProperty(keyNames).GetValue(entry.Entity).ToString();
                                 //model.value.v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString();
                                 //DictionaryStore.Add(a.GetProperty(keyNames).GetValue(entry.Entity).ToString() + name, entry.Entity);
                             }
                         }
                     }
                 }

                 //foreach (DbEntityEntry entry in objects
                 //    .Where(p => p.State == EntityState.Deleted))
                 //{
                 //    if (entry.Entity != null)
                 //    {
                 //        entry.Entity.GetType().GetProperty("Deleted_By").SetValue(entry.Entity, UserName);
                 //        entry.Entity.GetType().GetProperty("Deleted_Date").SetValue(entry.Entity, DateTime.Now);
                 //    }
                 //}

                 return model;
             }
         }

         //protected override void OnModelCreating(DbModelBuilder builder)
         //{
         //    base.OnModelCreating(builder);
         //    builder.Entity<SanPham_DuAn>().Ignore(m => m.Created_By);

         //    //builder.Types<EntityType>().Configure(config =>
         //    //    config.Ignore(m => m.GetType().GetProperty("Created_By"))
         //    //    .Ignore(m => m.GetType().GetProperty("Created_Date"))
         //    //);


         //}
     }*/
}