﻿using ApiGDTVietnam.DataProvider.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;

namespace ApiGDTVietnam.Controllers.TestController.Ext
{
   // [Authorize]
    [RoutePrefix("TaiKhoans")]

    public class TaiKhoanController : ApiController
    {

        private SeeModels db = new SeeModels();


        //Find all roles for one user.
        [HttpGet]
        [Route("FindAllRoleByUserId")]
        public HttpResponseMessage GetRoleUser(Guid TaiKhoan_Id)
        {
            //Guid TaiKhoan_Id
            //Find 
            var _item = db.SYS_TaiKhoanTS.Where(w => w.Role_Id.Equals("F0A41161-469C-4C41-9A2B-589906937ADE")&& w.Role_Id == TaiKhoan_Id).Select(s => new
            {
                Role_Id = s.Role_Id,
                UserName = s.Name,
                Password = s.Password,
                Level = s.Level,
                Code = s.Code
            }).OrderBy(o => o.Role_Id).ToList();


            return Request.CreateResponse(HttpStatusCode.OK, _item);
        }

        [HttpGet]
        [Route("FindAllRoleByAdmin")]
        public HttpResponseMessage GetAdmin()
        {

            //Find 
            var _item = db.SYS_TaiKhoanTS.Where(w=>w.Name.Equals("admin")).Select(s => new
            {
                UserName = s.Name,
                Password = s.Password
            }).OrderBy(o=>o.UserName).ToList();


            return Request.CreateResponse(HttpStatusCode.OK, _item);
        }

    }
}