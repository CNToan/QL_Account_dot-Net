﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.TestController
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<TaiKhoan>("TaiKhoans");
    builder.EntitySet<Role>("Roles"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class TaiKhoansController : ODataController
    {
        private SeeModels db = new SeeModels();

        // GET: odata/TaiKhoans
        [EnableQuery]
        public IQueryable<SYS_TaiKhoanTS> GetTaiKhoans()
        {
            return db.SYS_TaiKhoanTS;
        }

        // GET: odata/TaiKhoans(5)
        [EnableQuery]
        public SingleResult<SYS_TaiKhoanTS> GetTaiKhoan([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_TaiKhoanTS.Where(taiKhoan => taiKhoan.Id == key));
        }

        // PUT: odata/TaiKhoans(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_TaiKhoanTS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_TaiKhoanTS taiKhoan = db.SYS_TaiKhoanTS.Find(key);
            if (taiKhoan == null)
            {
                return NotFound();
            }

            patch.Put(taiKhoan);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaiKhoanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(taiKhoan);
        }

        // POST: odata/TaiKhoans
        public IHttpActionResult Post(SYS_TaiKhoanTS taiKhoan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_TaiKhoanTS.Add(taiKhoan);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TaiKhoanExists(taiKhoan.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(taiKhoan);
        }

        // PATCH: odata/TaiKhoans(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_TaiKhoanTS> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_TaiKhoanTS taiKhoan = db.SYS_TaiKhoanTS.Find(key);
            if (taiKhoan == null)
            {
                return NotFound();
            }

            patch.Patch(taiKhoan);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaiKhoanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(taiKhoan);
        }

        // DELETE: odata/TaiKhoans(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_TaiKhoanTS taiKhoan = db.SYS_TaiKhoanTS.Find(key);
            if (taiKhoan == null)
            {
                return NotFound();
            }

            db.SYS_TaiKhoanTS.Remove(taiKhoan);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/TaiKhoans(5)/Role
        [EnableQuery]
        public SingleResult<SYS_ROLETS> GetRole([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_TaiKhoanTS.Where(m => m.Id == key).Select(m => m.SYS_ROLETS));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TaiKhoanExists(Guid key)
        {
            return db.SYS_TaiKhoanTS.Count(e => e.Id == key) > 0;
        }
    }
}
