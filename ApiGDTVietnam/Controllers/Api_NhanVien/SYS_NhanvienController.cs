﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.Api_NhanVien
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SYS_Nhanvien>("SYS_Nhanvien");
    builder.EntitySet<SYS_ChucVu>("SYS_ChucVu"); 
    builder.EntitySet<SYS_CTy>("SYS_CTy"); 
    builder.EntitySet<SYS_KinhNghiem>("SYS_KinhNghiem"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SYS_NhanvienController : ODataController
    {
        private DatModelEntities1 db = new DatModelEntities1();

        // GET: odata/SYS_Nhanvien
        [EnableQuery]
        public IQueryable<SYS_Nhanvien> GetSYS_Nhanvien()
        {
            return db.SYS_Nhanvien;
        }

        // GET: odata/SYS_Nhanvien(5)
        [EnableQuery]
        public SingleResult<SYS_Nhanvien> GetSYS_Nhanvien([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_Nhanvien.Where(sYS_Nhanvien => sYS_Nhanvien.Id == key));
        }

        // PUT: odata/SYS_Nhanvien(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_Nhanvien> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_Nhanvien sYS_Nhanvien = db.SYS_Nhanvien.Find(key);
            if (sYS_Nhanvien == null)
            {
                return NotFound();
            }

            patch.Put(sYS_Nhanvien);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_NhanvienExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_Nhanvien);
        }

        // POST: odata/SYS_Nhanvien
        public IHttpActionResult Post(SYS_Nhanvien sYS_Nhanvien)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_Nhanvien.Add(sYS_Nhanvien);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SYS_NhanvienExists(sYS_Nhanvien.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sYS_Nhanvien);
        }

        // PATCH: odata/SYS_Nhanvien(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_Nhanvien> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_Nhanvien sYS_Nhanvien = db.SYS_Nhanvien.Find(key);
            if (sYS_Nhanvien == null)
            {
                return NotFound();
            }

            patch.Patch(sYS_Nhanvien);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_NhanvienExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_Nhanvien);
        }

        // DELETE: odata/SYS_Nhanvien(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_Nhanvien sYS_Nhanvien = db.SYS_Nhanvien.Find(key);
            if (sYS_Nhanvien == null)
            {
                return NotFound();
            }

            db.SYS_Nhanvien.Remove(sYS_Nhanvien);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

       /* // GET: odata/SYS_Nhanvien(5)/SYS_ChucVu
        [EnableQuery]
        public SingleResult<SYS_ChucVu> GetSYS_ChucVu([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_Nhanvien.Where(m => m.Id == key).Select(m => m.SYS_ChucVu));
        }*/

        // GET: odata/SYS_Nhanvien(5)/SYS_CTy
        [EnableQuery]
        public SingleResult<SYS_CTy> GetSYS_CTy([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_Nhanvien.Where(m => m.Id == key).Select(m => m.SYS_CTy));
        }

        // GET: odata/SYS_Nhanvien(5)/SYS_KinhNghiem
        [EnableQuery]
        public SingleResult<SYS_KinhNghiem> GetSYS_KinhNghiem([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_Nhanvien.Where(m => m.Id == key).Select(m => m.SYS_KinhNghiem));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYS_NhanvienExists(Guid key)
        {
            return db.SYS_Nhanvien.Count(e => e.Id == key) > 0;
        }
    }
}
