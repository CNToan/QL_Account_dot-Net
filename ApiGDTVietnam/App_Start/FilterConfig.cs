﻿using System.Web.Mvc;

namespace ApiGDTVietnam
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new LogAttribute());
        }
    }
}
